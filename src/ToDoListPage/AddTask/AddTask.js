import React from 'react';
import {Button, Modal} from "react-bootstrap";
import {FieldTypes, reduxFormField} from "../../common/redux-form-field";
import {Field, Form, getFormValues, reduxForm} from "redux-form";
import {actions} from "../actions";
import './AddTask.css';

export const FORM_NAME = 'AddTask';

class AddTask extends React.Component {

    constructor() {
        super();
        this.state = {showModal: false};
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.preview = this.preview.bind(this);
    }

    componentDidMount() {
        this.setState({showModal: true});
    }

    componentWillUnmount() {
        this.setState({showModal: false});
    }

    close() {
        this.setState({showModal: false});
        this.back();
    }

    back() {
        // TODO: bit hardcoded... need to think about it
        this.props.history.push('/');
    }

    open() {
        this.setState({showModal: true});
    }

    preview() {
        const {formValues} = this.props;
        formValues && this.props.preview(formValues);
    }

    render() {
        const {handleSubmit, pristine, submitting, showPreview, closePreview} = this.props;

        return <Form onSubmit={handleSubmit}>
            <Modal show={this.state.showModal && !showPreview} onHide={this.close}>
                <Modal.Header>
                    <Modal.Title>Add task</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Field name="username" label="Username" type={FieldTypes.text} component={reduxFormField}/>
                    <Field name="email" label="Email" type={FieldTypes.email} component={reduxFormField}/>
                    <Field name="text" label="Text" type={FieldTypes.textarea} component={reduxFormField} rows={3}/>
                    <Field name="image" label="Image" type={FieldTypes.image} component={reduxFormField}/>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.close}>Close</Button>
                    <Button onClick={this.preview}>Preview</Button>
                    <Button type="submit" onClick={handleSubmit(actions.saveTask)}
                            disabled={pristine || submitting}>Save</Button>
                </Modal.Footer>
            </Modal>
            {showPreview ? <div className="task-preview">
                <Button onClick={closePreview}> Close preview </Button>
                <Button bsClass="btn btn-primary pull-right" type="submit" onClick={handleSubmit(actions.saveTask)}
                        disabled={pristine || submitting}>Save</Button>
            </div> : null}
        </Form>
    }

}

export default reduxForm({
    form: FORM_NAME,
    destroyOnUnmount: false
})(AddTask);