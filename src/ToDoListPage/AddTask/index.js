import AddTask, {FORM_NAME} from "./AddTask";
import {connect} from "react-redux";
import {moduleId} from "../module-id";
import {bindActionCreators} from "redux";
import {actions} from "../actions";
import {getFormValues} from "redux-form";


const mapStateToProps = rootState => {
    const state = rootState[moduleId];
    const formValues = getFormValues(FORM_NAME)(rootState);
    const showPreview = state.get('showPreview');

    return {
        formValues,
        showPreview
    };
};

const mapDispatchToProps = dispatch => {
    return {
        preview: bindActionCreators((payload) => actions.preview(payload), dispatch),
        closePreview: bindActionCreators((payload) => actions.closePreview(payload), dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTask);