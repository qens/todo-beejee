import {delay, throttle} from "redux-saga";
import {call, fork, select, put, take, takeLatest, takeEvery} from 'redux-saga/effects'
import {actionTypes} from "./action-types";
import apiClient from '../common/api-client';
import {BASE_URL, createAppUrl} from "../common/url";
import {SubmissionError} from "redux-form";
import {actions} from "./actions";
import * as md5 from "md5";
import {moduleId} from "./module-id";

function* getToDoList(action) {
    console.log('saga', action);

    const {sortField, sortDirection, page} = action.payload;

    const params = {
        sort_field: sortField,
        sort_direction: sortDirection,
        page
    };
    try {
        const message = yield apiClient.get(createAppUrl(`${BASE_URL}/`, params));

        yield put({
            type: actionTypes.GET_TODO_LIST_SUCCESS,
            payload: {
                tasks: message.tasks,
                totalTaskCount: message.total_task_count,
                sortField, sortDirection, page
            }
        });
    } catch (err) {
        yield put({
            type: actionTypes.GET_TODO_LIST_ERROR,
            payload: err
        })
    }
}

function* saveTask(action) {

    try {
        const {username, email, text, image} = action.payload;

        const data = new FormData();
        username && data.append('username', username);
        email && data.append('email', email);
        text && data.append('text', text);
        image && data.append('image', image[0]);

        const message = yield apiClient.post(createAppUrl(`${BASE_URL}/create`), data);
        yield put(actions.saveTask.success(message));
    } catch (err) {
        err = err.message ? new SubmissionError(err.message) : err;
        yield put(actions.saveTask.failure(err));
    }
}

function* preview(action) {

    const {username, email, text, image: _image} = action.payload;
    const id = -1;
    const image_path = _image ? window.URL.createObjectURL(_image[0]) : _image;
    const payload = {id, username, email, text, image_path};

    try {
        yield put({type: actionTypes.ADD_PREVIEWED_TASK, payload});

        yield take([actionTypes.CLOSE_PREVIEW, actions.saveTask.REQUEST]);
        // yield take('*');

    } finally {
        yield put({type: actionTypes.REMOVE_PREVIEWED_TASK, payload});
        yield put(actions.closePreview(payload));
    }
}

function* editTask(action) {
    const rootState = yield select();
    const state = rootState[moduleId];
    const {id, text, status: _status} = action.payload;
    const task = state.get('tasks').find((value) => value.get('id') === id);
    const status = !!_status ? 10 : 0;

    const token = 'beejee';

    const signature = md5(`status=${status}&text=${text}&token=${token}`);
    try {
        yield put({type: actionTypes.EDIT_TASK_SUCCESS, payload: {id, text, status}});
        yield call([apiClient, apiClient.post], createAppUrl(`${BASE_URL}/edit/${id}`),
            {token, signature, text, status});
    } catch (err) {
        yield put({
            type: actionTypes.EDIT_TASK_ERROR, payload: {
                id: task.get('id'),
                text: task.get('text'),
                status: task.get('status')
            }
        });
        yield  put({
            type: actionTypes.UPDATE_ERROR,
            payload: err.message || err
        })
    }

}

export default function* () {
    yield takeLatest(actionTypes.GET_TODO_LIST, getToDoList);
    yield takeEvery(actions.saveTask.REQUEST, saveTask);
    yield takeLatest(actionTypes.PREVIEW, preview);
    yield throttle(500, actionTypes.EDIT_TASK, editTask)
}