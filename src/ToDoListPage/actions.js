import {actionTypes} from "./action-types";
import {createFormAction} from "redux-form-saga";

export const actions = {
    getToDoList: (payload) => ({
        type: actionTypes.GET_TODO_LIST,
        payload
    }),

    preview: (payload) => ({
        type: actionTypes.PREVIEW,
        payload
    }),

    closePreview: (payload) => ({
        type: actionTypes.CLOSE_PREVIEW,
        payload
    }),

    saveTask: createFormAction(actionTypes.SAVE_TASK),

    editTask: (payload) => ({
        type: actionTypes.EDIT_TASK,
        payload
    })
};

