import {bindActionCreators} from 'redux';
import {actions} from "./actions";
import {ToDoListPage} from "./ToDoListPage";
import {connect} from "react-redux";
import {Map, List, fromJS} from 'immutable';
import {moduleId} from "./module-id";
import {moduleId as loginModuleId} from '../Login/module-id';

const mapStateToProps = (rootState, props) => {
    const state = rootState[moduleId];
    const tasks = state.get('tasks');
    const totalTaskCount = state.get('totalTaskCount');
    const saving = state.get('saving');
    const error = state.get('error');
    const sortField = state.get('sortField');
    const sortDirection = state.get('sortDirection');
    const page = state.get('page');
    const tasksPerPage = state.get('tasksPerPage');
    const showAddTaskDialog = state.get('showAddTaskDialog');
    const pagesCount = Math.ceil(totalTaskCount / tasksPerPage);

    const loginState = rootState[loginModuleId];
    const username = loginState.get('username');

    return {
        tasks,
        totalTaskCount,
        sortField,
        sortDirection,
        page,
        pagesCount,
        showAddTaskDialog,
        username,
        saving,
        error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getToDoList: bindActionCreators((payload) => actions.getToDoList(payload), dispatch),
        editTask: bindActionCreators((payload) => actions.editTask(payload), dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDoListPage);