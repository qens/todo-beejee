import React from 'react';
import {Button, ButtonGroup, ButtonToolbar, Col, Glyphicon, Grid, Panel, Row, Table} from "react-bootstrap";
import {Link, Route} from "react-router-dom";
import AddTask from "./AddTask";
import Login from "../Login";

const ASC = 'asc';
const DESC = 'desc';

export class ToDoListPage extends React.Component {

    componentDidMount() {
        const {sortField, sortDirection, page} = this.props;
        this.props.getToDoList({sortField, sortDirection, page});
    }

    componentWillReceiveProps({showAddTaskDialog}) {
        if (showAddTaskDialog !== this.props.showAddTaskDialog) {
            showAddTaskDialog ? this.props.history.push('/add') : this.props.history.push('/');
        }
    }

    glyph(field) {
        const {sortField, sortDirection, page} = this.props;
        return field === sortField ?
            sortDirection === 'asc' ? <Glyphicon glyph="sort-by-alphabet"
                                                 onClick={() => this.props.getToDoList({
                                                     sortField,
                                                     sortDirection: DESC,
                                                     page
                                                 })}/> :
                <Glyphicon glyph="sort-by-alphabet-alt"
                           onClick={() => this.props.getToDoList({sortField, sortDirection: ASC, page})}/>
            : <Glyphicon glyph="sort"
                         onClick={() => this.props.getToDoList({sortField: field, sortDirection: ASC, page})}/>;
    }

    buttons() {
        const {pagesCount, sortField, sortDirection, page, getToDoList} = this.props;
        const buttons = [];
        for (let i = 1; i <= pagesCount; i++) {
            buttons.push(<Button bsClass={i === page ? 'btn btn-primary' : 'btn'}
                                 onClick={() => getToDoList({sortField, sortDirection, page: i})}
                                 key={i}>{i}</Button>);
        }
        return buttons;
    }

    renderLogin() {
        const {username} = this.props;
        return username ?
            <div>
                {username} <Button onClick={() => this.props.logout()}>Logout</Button>
            </div> : <React.Fragment><Link to="/login">
                <Button>Login</Button>
            </Link><Route path="/login" component={Login}/></React.Fragment>;
    }

    handleChange(ev, task, fieldName) {
        const value = ev.target.value || ev.target.checked;
        if (value !== task.get(fieldName)) {
            const text = fieldName === 'text' ? value : task.get('text');
            const status = fieldName === 'status' ? value : task.get('status');
            const id = task.get('id');
            this.props.editTask({
                id, text, status
            })
        }
    }

    render() {
        const {
            tasks, username, saving, error
        } = this.props;
        const isAdmin = !!username;

        return <React.Fragment>
            <Grid>
                <Row>
                    <Col md={12}>
                        <div className="pull-right">{this.renderLogin()}</div>
                        {error ? <Panel bsStyle="danger">{error}</Panel> : null}
                    </Col>
                </Row>
                <Row>
                    <Col md={7}><h1>TODO list {saving ? 'saving...' : null}</h1></Col>
                    <Col md={5}>
                        <div className="pull-right">
                            <Link to='/add'><Button>Add new task</Button></Link></div>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <Table striped bordered condensed hover>
                            <thead>
                            <tr>
                                <th>Username {this.glyph('username')}</th>
                                <th>Email {this.glyph('email')}</th>
                                <th>Task</th>
                                <th>Status {this.glyph('status')}</th>
                                <th>Attachment</th>
                            </tr>
                            </thead>
                            <tbody>
                            {tasks && tasks.map(task => <tr key={task.get('id')}>
                                <td>{task.get('username')}</td>
                                <td>{task.get('email')}</td>
                                <td>{isAdmin ?
                                    <textarea type="textarea" value={task.get('text')}
                                              onChange={ev => this.handleChange(ev, task, 'text')}
                                              rows="2"/> : task.get('text')}</td>
                                <td><input type="checkbox" checked={task.get('status')}
                                           onChange={ev => this.handleChange(ev, task, 'status')} disabled={!isAdmin}/>
                                </td>
                                <td><img style={{maxWidth: 320, maxHeight: 240}} src={task.get('image_path')}/></td>
                            </tr>)}
                            </tbody>
                        </Table>
                        <ButtonToolbar>
                            <ButtonGroup>
                                {this.buttons()}
                            </ButtonGroup>
                        </ButtonToolbar>
                    </Col>
                </Row>
            </Grid>
            <Route path='/add' component={AddTask}/>
        </React.Fragment>;
    }
}