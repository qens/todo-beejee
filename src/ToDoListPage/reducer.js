import {actionTypes} from './action-types';
import {Map, List, fromJS} from 'immutable';
import {actions} from "./actions";

const initialState = fromJS({
    tasks: [],
    totalTaskCount: 0,
    sortField: 'id',
    sortDirection: 'asc',
    page: 1,
    tasksPerPage: 3,
    showAddTaskDialog: false,
    showPreview: false,
    saving: false
});

function updateTask(tasks, payload) {
    const {id, text, status} = payload;

    return tasks.update(tasks.findIndex((task) => {
            console.log('findIndex', task.toJS());
            return task.get('id') === id
        }),
        task => {
            console.log('should be task', task);
            return task.set('text', text)
                .set('status', status)
        });
}

export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_TODO_LIST_SUCCESS:
            const {tasks, totalTaskCount, sortField, sortDirection, page} = action.payload;
            return state.set('tasks', fromJS(tasks))
                .set('totalTaskCount', totalTaskCount)
                .set('sortDirection', sortDirection)
                .set('page', page)
                .set('sortField', sortField);

        case actions.saveTask.SUCCESS:
            return state.update('tasks', tasks => tasks.unshift(fromJS(action.payload)))
                .set('showAddTaskDialog', false)
                .set('showPreview', false);

        case actions.saveTask.FAILURE:
            return state.set('showAddTaskDialog', true)
                .set('showPreview', false);

        case actionTypes.ADD_PREVIEWED_TASK:
            return state.update('tasks', tasks => tasks.unshift(fromJS(action.payload)));

        case actionTypes.REMOVE_PREVIEWED_TASK:
            return state.update('tasks', tasks => tasks.shift());

        case actionTypes.PREVIEW:
            return state.set('showPreview', true);

        case actionTypes.CLOSE_PREVIEW:
            return state.set('showPreview', false)

        case actionTypes.EDIT_TASK:
            return state.set('saving', true);

        case actionTypes.EDIT_TASK_SUCCESS:
            return state.set('error', null)
                .set('tasks', updateTask(state.get('tasks'), action.payload))
                .set('saving', false);

        case actionTypes.EDIT_TASK_ERROR:
            return state.set('tasks', updateTask(state.get('tasks'), action.payload))
                .set('saving', false);

        case actionTypes.UPDATE_ERROR:
            return state.set('error', action.payload);


        default:
            return state;
    }
}