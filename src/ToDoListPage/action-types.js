import {moduleId} from "./module-id";

export const actionTypes = {
    GET_TODO_LIST: `${moduleId}/GET_TODO_LIST`,
    GET_TODO_LIST_SUCCESS: `${moduleId}/GET_TODO_LIST_SUCCESS`,
    GET_TODO_LIST_ERROR: `${moduleId}/GET_TODO_LIST_ERROR`,
    SAVE_TASK: `${moduleId}/SAVE_TASK`,
    EDIT_TASK: `${moduleId}/EDIT_TASK`,
    EDIT_TASK_SUCCESS: `${moduleId}/EDIT_TASK_SUCCESS`,
    EDIT_TASK_ERROR: `${moduleId}/EDIT_TASK_ERROR`,
    PREVIEW: `${moduleId}/PREVIEW`,
    CLOSE_PREVIEW: `${moduleId}/CLOSE_PREVIEW`,
    ADD_PREVIEWED_TASK: `${moduleId}/ADD_PREVIEWED_TASK`,
    REMOVE_PREVIEWED_TASK: `${moduleId}/REMOVE_PREVIEWED_TASK`,
    UPDATE_ERROR: `${moduleId}/UPDATE_ERROR`
};
