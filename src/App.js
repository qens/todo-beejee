import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ToDoListPage from "./ToDoListPage";
import {Route} from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Route path='/' component={ToDoListPage} />
      </div>
    );
  }
}

export default App;
