import {createStore, applyMiddleware, combineReducers} from 'redux';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';
import {default as toDoListPageSaga} from './ToDoListPage/saga';
import {default as toDoListPageReducer} from './ToDoListPage/reducer';
import {moduleId as toDoListPageModuleId} from './ToDoListPage/module-id';
import {default as loginSaga} from './Login/saga';
import {default as loginReducer} from './Login/reducer';
import {moduleId as loginModuleId} from './Login/module-id';
import {reducer as formReducer} from 'redux-form'
import formActionSaga from 'redux-form-saga';
import {actions} from "./ToDoListPage/actions";
import {FORM_NAME as addTaskFormName} from './ToDoListPage/AddTask/AddTask';

const createAppStore = (reducer, emmiterMiddleware) => {
    const sagaMiddleware = createSagaMiddleware();

    const store = createStore(reducer, undefined, applyMiddleware(logger, sagaMiddleware));

    sagaMiddleware.run(toDoListPageSaga);
    sagaMiddleware.run(loginSaga);
    sagaMiddleware.run(formActionSaga);

    return store;
};


export const rootReducer = combineReducers({
    [toDoListPageModuleId]: toDoListPageReducer,
    [loginModuleId]: loginReducer,
    form: formReducer.plugin({
        [addTaskFormName]: (state, action) => {
            switch (action.type) {
                case actions.saveTask.SUCCESS :
                    return null;
                default:
                    return state;
            }
        }
    })
});

export default () => {
    const store = createAppStore(rootReducer, null);

    return store;
}