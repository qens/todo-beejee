import {delay} from "redux-saga";
import {call, put, take, takeLatest, takeEvery} from 'redux-saga/effects'
import {actionTypes} from "./action-types";
import apiClient from '../common/api-client';
import {BASE_URL, createAppUrl} from "../common/url";
import {SubmissionError} from "redux-form";
import {actions} from "./actions";

function* login(action){

    const {username, password} = action.payload;

    if (username === 'admin' && password === '123') {
        yield put(actions.login.success(username));
    } else {
        yield put(actions.login.failure(new SubmissionError({
            username: 'Incorrect username or password',
            password: 'Incorrect username or password'
        })));
    }
}

export default function* () {
    yield takeEvery(actions.login.REQUEST, login);
}