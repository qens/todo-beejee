import {moduleId} from "./module-id";

export const actionTypes = {
    LOGIN: `${moduleId}/LOGIN`,
    LOGOUT: `${moduleId}/LOGOUT`,
};
