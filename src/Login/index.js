import Login from "./Login";

import {connect} from "react-redux";
import {moduleId} from "./module-id";

const mapStateToProps = rootState => {
    const state = rootState[moduleId];
    const username = state.get('username');

    return {
        username
    };
};

export default connect(mapStateToProps, null)(Login);