import {actionTypes} from './action-types';
import {Map, List, fromJS} from 'immutable';
import {actions} from "./actions";

const initialState = fromJS({
    username: null
});

export default (state = initialState, action) => {
    switch (action.type) {

        case actions.login.SUCCESS:
            return state.set('username', action.payload);

        case actions.login.FAILURE:
        case actionTypes.LOGOUT:
            return state.set('username', null);

        default:
            return state;
    }
}