import {actionTypes} from "./action-types";
import {createFormAction} from "redux-form-saga";

export const actions = {
    login: createFormAction(actionTypes.LOGIN),
    logout: () => {
        type: actionTypes.LOGOUT
    }
};

