import * as React from "react";
import {actions} from "./actions";
import {FieldTypes, reduxFormField} from "../common/redux-form-field";
import {Field, Form, reduxForm} from "redux-form";
import {Button, Modal} from "react-bootstrap";

export const FORM_NAME = 'Login';

class Login extends React.Component {

    constructor() {
        super();
        this.state = {showModal: true};
        this.close = this.close.bind(this);
        this.back = this.back.bind(this);
    }

    componentWillReceiveProps({username}) {
        // TODO some kinda hack...
        username && this.back();
    }

    componentDidMount() {
        this.setState({showModal: true});
    }

    componentWillUnmount() {
        this.setState({showModal: false});
    }

    close() {
        this.setState({showModal: false});
        this.back();
    }

    back() {
        // TODO: bit hardcoded... need to think about it
        this.props.history.push('/');
    }

    render() {
        const {handleSubmit, pristine, submitting} = this.props;

        return <Form onSubmit={handleSubmit}>
            <Modal show={this.state.showModal} onHide={this.close}>
                <Modal.Header>
                    <Modal.Title>Login</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Field name="username" label="Username" type={FieldTypes.text} component={reduxFormField}/>
                    <Field name="password" label="Password" type={FieldTypes.password} component={reduxFormField}/>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.close}>Close</Button>
                    <Button type="submit" onClick={handleSubmit(actions.login)}
                            disabled={pristine || submitting}>Login</Button>
                </Modal.Footer>
            </Modal>
        </Form>
    }
}

export default reduxForm({
    form: FORM_NAME
})(Login);