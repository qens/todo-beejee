import transform from 'lodash/transform';

const DEVELOPER_NAME = 'Daniyar_Bulgunayev';
export const BASE_URL = 'https://uxcandy.com/~shapoval/test-task-backend';

export class Url {

    constructor(url = BASE_URL, params = {}) {
        this.url = url;
        this.params = params;
    }

    addParam({key, value}) {
        this.params[key] = value;
    }

    toString() {
        let paramsStr = '?';

        paramsStr += transform(this.params, (result, value, key) => {
            result.push(`${key}=${value}`);
        }, []).join('&');

        return this.url + paramsStr;
    }
}

export function createAppUrl(url = BASE_URL, params = {}) {
    params['developer'] = DEVELOPER_NAME;
    return new Url(url, params);
}